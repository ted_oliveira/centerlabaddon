﻿using System.Windows.Forms;

namespace CenterlabAddon
{
    class FrmConfiguracoes
    {
        #region :: Propriedades

        public SAPbouiCOM.Application SBOApplication;
        public SAPbobsCOM.Company oCompany;
        public SAPbouiCOM.Form oForm;
        public SAPbouiCOM.DBDataSource dbdts;
        public FrmConfiguracoes(SAPbouiCOM.Application SBOApplication, SAPbobsCOM.Company oCompany)
        {
            this.SBOApplication = SBOApplication;
            this.oCompany = oCompany;

            SBOApplication.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(ItemEvent);

            this.CriarForm();

            oForm.Visible = true;

            SaveAsXML();
        }

        #endregion


        #region :: Criação do Formulário

        private void CriarForm()
        {
            SAPbouiCOM.Item oItem = null;

            // *******************************************
            // we will use the following objects to set
            // the specific values of every item
            // we add.
            // this is the best way to do so
            //*********************************************

            SAPbouiCOM.Button oButton = null;
            SAPbouiCOM.StaticText oStaticText = null;
            SAPbouiCOM.EditText oEditText = null;

            string dbdatasource_name = "@U_CONF";

            // add a new form
            SAPbouiCOM.FormCreationParams oCreationParams = null;

            oCreationParams = ((SAPbouiCOM.FormCreationParams)(SBOApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));

            oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;
            oCreationParams.UniqueID = "FrmConfig";

            oForm = SBOApplication.Forms.AddEx(oCreationParams);

            // add a Data Source to the form
            dbdts = oForm.DataSources.DBDataSources.Add(dbdatasource_name);

            // set the form properties
            oForm.Title = "Configurações de Vencimento";
            oForm.Left = 440;
            oForm.Top = 80;
            oForm.ClientHeight = 90;
            oForm.ClientWidth = 430;

            //*****************************************
            // Adding Items to the form
            // and setting their properties
            //*****************************************


            //**********************
            // Adding an Ok button
            //*********************

            // We get automatic event handling for
            // the Ok and Cancel Buttons by setting
            // their UIDs to 1 and 2 respectively

            oItem = oForm.Items.Add("1", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oItem.Left = 6;
            oItem.Width = 65;
            oItem.Top = 51;
            oItem.Height = 19;

            oButton = ((SAPbouiCOM.Button)(oItem.Specific));

            oButton.Caption = "Ok";

            //************************
            // Adding a Cancel button
            //***********************

            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oItem.Left = 75;
            oItem.Width = 65;
            oItem.Top = 51;
            oItem.Height = 19;

            oButton = ((SAPbouiCOM.Button)(oItem.Specific));

            oButton.Caption = "Cancel";

            //************************
            // Adding a Rectangle
            //***********************

            oItem = oForm.Items.Add("Rect1", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE);
            oItem.Left = 0;
            oItem.Width = 430;
            oItem.Top = 1;
            oItem.Height = 89;

            //***************************
            // Adding a Static Text item
            //***************************

            oItem = oForm.Items.Add("StaticTxt1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oItem.Left = 7;
            oItem.Width = 148;
            oItem.Top = 20;
            oItem.Height = 14;

            oItem.LinkTo = "EditText1";

            oStaticText = ((SAPbouiCOM.StaticText)(oItem.Specific));

            oStaticText.Caption = "Dia Vencimento Lote";


            //*************************
            // Adding a Text Edit item
            //*************************

            oItem = oForm.Items.Add("EditText1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oItem.Left = 157;
            oItem.Width = 163;
            oItem.Top = 20;
            oItem.Height = 14;

            oEditText = ((SAPbouiCOM.EditText)(oItem.Specific));

            // bind the text edit item to the defined used data source
            oEditText.DataBind.SetBound(true, dbdatasource_name, "U_US_QTDDIAS");

            SAPbouiCOM.Conditions oConds = SBOApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
            SAPbouiCOM.Condition oCond = oConds.Add();

            oCond.Alias = "Code";
            oCond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
            oCond.CondVal = "1";

            dbdts.Query(oConds);
        }

        private void SaveAsXML()
        {
            //**********************************************************************
            //
            // always use XML to work with user forms.
            // after creating your form save it as an XML file
            //
            //**********************************************************************

            System.Xml.XmlDocument oXmlDoc = null;

            oXmlDoc = new System.Xml.XmlDocument();

            string sXmlString = null;

            // get the form as an XML string
            sXmlString = oForm.GetAsXML();

            // load the form's XML string to the
            // XML document object
            oXmlDoc.LoadXml(sXmlString);

            // save the XML Document
            string sPath = null;

            sPath = System.IO.Directory.GetParent(Application.StartupPath).ToString();

            oXmlDoc.Save((sPath + @"\MySimpleForm.xml"));

        }

        #endregion


        #region :: Eventos

        private void ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (pVal.BeforeAction && pVal.ItemUID == "1" && pVal.FormUID == "FrmConfig")
            {
                string sql = "SELECT Count(U_US_QTDDIAS) 'Qdias' FROM [@U_CONF] WHERE CODE = 1 ";

                SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(sql);

                int qtdregistro = (int)rs.Fields.Item("Qdias").Value;
                string qtddias = dbdts.GetValue("U_US_QTDDIAS", 0);
                if (qtdregistro == 0)
                {

                    sql = $"INSERT INTO [@U_CONF](Code,Name,U_US_QTDDIAS) VALUES (1,1,'{qtddias}')";
                }
                else
                {
                    sql = $"UPDATE [@U_CONF] SET U_US_QTDDIAS = '{qtddias}' WHERE CODE = 1";

                }

                rs.DoQuery(sql);
            }
        }

        #endregion
    }
}
