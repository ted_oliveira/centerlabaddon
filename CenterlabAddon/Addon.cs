﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CenterlabAddon
{
    class Addon : IDisposable
    {
        #region :: Propriedades Globais

        private SAPbouiCOM.Application SBOApplication;
        private SAPbobsCOM.Company oCompany;
        private string AddonName = "Centerlab - Seleção de Lotes";
        private SAPbouiCOM.EventFilters oEventFilters = null;
        private SAPbouiCOM.EventFilter oEventFilter = null;

        #endregion


        #region :: Inicialização 

        public Addon()
        {
            //SetApplication();


            var SboGuiApi = new SAPbouiCOM.SboGuiApi();
            var connectionString = string.Empty;
            try
            {
                if (Environment.GetCommandLineArgs().Length > 1)
                    connectionString = Environment.GetCommandLineArgs().GetValue(1).ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show("Não foi possível buscar a string de conexão com o SAP.\nErro: " + e.Message);
            }

            SboGuiApi.Connect(connectionString);

            SBOApplication = SboGuiApi.GetApplication();
            oCompany = SBOApplication.Company.GetDICompany();

            Dialogs.SBOApplication = SBOApplication;
            Dialogs.Info(":: " + AddonName + " :: Iniciando ...");

            /*
            Dialogs.Info(":: " + AddonName + " :: Conectando com DI API ...");
            if (SetConnectionContext() != 0)
            {
                Dialogs.Error(":: " + AddonName + " :: Falha ao conectar com DI API ", true);
                System.Windows.Forms.Application.Exit();
            }

            Dialogs.Info(":: " + AddonName + " :: Conectando com Banco de Dados ...");
            if (ConnectToCompany() != 0)
            {
                Dialogs.Error(":: " + AddonName + " :: Falha ao conectar com o Banco de Dados", true);
                System.Windows.Forms.Application.Exit();
            }
            */

            Dialogs.Info(":: " + AddonName + " :: Criando estruturas de tabelas ...");

            new ManipulaCampos(SBOApplication, oCompany).CriarCamposAutomaticamente();

            Dialogs.Success(":: " + AddonName + " :: Inicializado com sucesso");

            CriaMenus();
            SetFilters();
        }

        private void SetApplication()
        {
            SAPbouiCOM.SboGuiApi sboGuiApi;
            string connectionString = null;
            sboGuiApi = new SAPbouiCOM.SboGuiApi();

            try
            {
                if (Environment.GetCommandLineArgs().Length > 1)
                    connectionString = Environment.GetCommandLineArgs().GetValue(1).ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show("Não foi possível buscar a string de conexão com o SAP.\nErro: " + e.Message);
            }

            try
            {
                sboGuiApi.Connect(connectionString);
            }
            catch (Exception e)
            {
                MessageBox.Show("Não foi possível estabelecer uma conexão com o SAP.\nErro: " + e.Message);
            }
            SBOApplication = sboGuiApi.GetApplication();
        }

        private int SetConnectionContext()
        {
            string cookie;
            string connectionContext = "";

            try
            {

                // First initialize the Company object
                oCompany = new SAPbobsCOM.Company();

                // Acquire the connection context cookie from the DI API.
                cookie = oCompany.GetContextCookie();

                // Retrieve the connection context string from the UI API using the acquired cookie.
                connectionContext = SBOApplication.Company.GetConnectionContext(cookie);

                // before setting the SBO Login Context make sure the company is not connected
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Falha ao tentar conectar com a DI API.\nErro: " + e.Message);
            }

            // Set the connection context information to the DI API.
            return oCompany.SetSboLoginContext(connectionContext);
        }

        private int ConnectToCompany()
        {
            // Establish the connection to the company database.
            return oCompany.Connect();
        }

        #endregion


        #region :: Gestão de Menu

        private void CriaMenus()
        {
            SAPbouiCOM.Menus menus;
            SAPbouiCOM.MenuItem menuItem = null;
            string updateMenu = "UPDTMENU";
            string menuID = "3328";

            //Coleção menus da aplicação...
            menus = SBOApplication.Menus;
            SAPbouiCOM.MenuCreationParams oCreationPackage;
            oCreationPackage = SBOApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);

            //id do menu de módulos...
            menuItem = BuscaMenu(menuID);
            menus = menuItem.SubMenus;

            try
            {
                //identifica o tipo do menu como nó ou folha da árvore...
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = updateMenu;
                oCreationPackage.String = "Update Suíte";
                oCreationPackage.Enabled = true;
                //oCreationPackage.Image = $@"{AppDomain.CurrentDomain.BaseDirectory}\UpdateIcon.png";
                //atribui o novo menu na quarta posição...
                oCreationPackage.Position = 3;
                menus.AddEx(oCreationPackage);
            }
            catch (Exception e)// Menu already exists
            { }

            menuItem = BuscaMenu(updateMenu);
            menus = menuItem.SubMenus;

            try
            {
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "FrmConfig";
                oCreationPackage.String = "Configuração de Vencimento";
                oCreationPackage.Enabled = true;
                oCreationPackage.Position = menus.Count + 1;
                menus.AddEx(oCreationPackage);
            }
            catch (Exception e) // Menu already exists
            { }
        }

        private SAPbouiCOM.MenuItem BuscaMenu(object menuID)
        {
            SAPbouiCOM.MenuItem retorno = null;
            try
            {
                retorno = SBOApplication.Menus.Item(menuID);
            }
            catch { }
            return retorno;
        }

        #endregion


        #region :: Captura de Eventos

        public void SetFilters()
        {
            oEventFilters = new SAPbouiCOM.EventFilters();

            oEventFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);
            oEventFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

            SAPbouiCOM.EventFilter oEventFilter = oEventFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
            oEventFilter.AddEx("139");
            oEventFilter.AddEx("133");

            oEventFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD).AddEx("139");
            oEventFilter = oEventFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
            oEventFilter.AddEx("139");
            oEventFilter.AddEx("42");

            SBOApplication.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(MenuEvent);
            SBOApplication.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(FormDataEvent);
            SBOApplication.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(ItemEvent);

            SBOApplication.SetFilter(oEventFilters);
        }

        private void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (EventoAntes(pVal) && pVal.MenuUID == "FrmConfig")
            {
                new FrmConfiguracoes(SBOApplication, oCompany);
            }
            // menu de cancelamento
            else if (EventoAntes(pVal) && pVal.MenuUID == "1284")
            {
                /** VERIFICANDO SE O USUÁRIO QUER CANCELAR. SE ELE QUISER, VERIFICA SE ELE PODE CANCELAR BASEADO SE JÁ GEROU PICKING OU NÃO **/
                SAPbouiCOM.Form oForm = SBOApplication.Forms.ActiveForm;
                if (oForm != null)
                {
                    oForm.Freeze(true);
                    try
                    {
                        // se for no pedido de venda
                        if (oForm.BusinessObject.Type == "139")
                        {
                            string docentry = oForm.DataSources.DBDataSources.Item("ORDR").GetValue("DocEntry", 0);
                            if (JaGerouPicking(docentry))
                            {
                                Dialogs.Error("Não é possível cancelar este pedido. O Picking já está sendo efetuado.\nCancelamento abortado.", true);
                                BubbleEvent = false;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Dialogs.Error("Erro interno. Erro ao verificar se o pedido pode ser cancelado.\nErro: " + e.Message, true);
                    }
                    finally
                    {
                        oForm.Freeze(false);
                    }
                }
                else
                {
                    Dialogs.Error("Erro interno. Formulário não encontrado", true);
                }

            }
        }

        private void FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD && EventoDepois(pVal))
            {
                /** DESABILITANDO A MATRIZ QUANDO FOR EDITAR UM PEDIDO. **/
                SAPbouiCOM.Form oForm = SBOApplication.Forms.Item(pVal.FormUID);
                if (oForm != null)
                {
                    oForm.Freeze(true);
                    try
                    {
                        if (UsuarioLogadoEVendedor())
                        {
                            oForm.Items.Item("38").Enabled = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Dialogs.Error("Erro interno. Erro ao bloquear a matriz de itens.\nErro: " + e.Message, true);
                    }
                    finally
                    {
                        oForm.Freeze(false);
                    }
                }
                else
                {
                    Dialogs.Error("Erro interno. Formulário não encontrado", true);
                }
            }
            else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && EventoAntes(pVal) && EventoEmAdicionandoPedidoVenda(pVal))
            {
                /* SELECIONA OS LOTES QUANDO FOR ADICIONAR UM NOVO PEDIDO */
                SAPbouiCOM.Form oForm = SBOApplication.Forms.Item(pVal.FormUID);
                if (oForm != null)
                {
                    oForm.Freeze(true);
                    try
                    {
                        Dialogs.Info("Validando Itens... Aguarde...");

                        string itemcode_duplicado = "";
                        Dictionary<string, string> itens = new Dictionary<string, string>() { };
                        SAPbouiCOM.Matrix mtx = oForm.Items.Item("38").Specific;
                        for (int i = 1; i <= mtx.RowCount; i++)
                        {
                            Dialogs.Info("Validando Itens... " + i + " de " + mtx.RowCount);

                            string itemcode = ((SAPbouiCOM.EditText)mtx.Columns.Item("1").Cells.Item(i).Specific).Value;
                            string itemname = ((SAPbouiCOM.EditText)mtx.Columns.Item("3").Cells.Item(i).Specific).Value;
                            string treetype = ((SAPbouiCOM.EditText)mtx.Columns.Item("39").Cells.Item(i).Specific).Value;
                            if (treetype == "I")
                            {
                                continue;
                            }
                            if (!itens.ContainsKey(itemcode))
                            {
                                itens.Add(itemcode, itemname);
                            }
                            else
                            {
                                itemcode_duplicado = itemcode;
                                break;
                            }

                            SBOApplication.RemoveWindowsMessage(SAPbouiCOM.BoWindowsMessageType.bo_WM_TIMER, true);
                        }

                        if (String.IsNullOrEmpty(itemcode_duplicado))
                        {
                            Dialogs.Info("Validação de itens finalizada.");
                        }
                        else
                        {
                            Dialogs.Error("O Item '" + itemcode_duplicado + " - " + itens[itemcode_duplicado] + "' está duplicado.", true);
                            BubbleEvent = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Dialogs.Error("Erro interno. Erro ao validar itens do pedido.\nErro: " + e.Message, true);
                    }
                    finally
                    {
                        oForm.Freeze(false);
                    }
                }
                else
                {
                    Dialogs.Error("Erro interno. Formulário não encontrado", true);
                }
            }
            else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && EventoDepois(pVal) && EventoEmAdicionandoPedidoVenda(pVal) && pVal.ActionSuccess)
            {
                SAPbouiCOM.Form oForm = SBOApplication.Forms.Item(pVal.FormUID);
                if (oForm != null)
                {
                    oForm.Freeze(true);
                    try
                    {
                        SelecionarLotes(pVal, SAPbobsCOM.BoObjectTypes.oOrders, "pedido");
                    }
                    catch (System.Runtime.InteropServices.COMException ex)
                    {
                        if (ex.ErrorCode == -2038)
                        {
                            Dialogs.Info("Tentando selecionar os lotes novamente...");
                            SelecionarLotes(pVal, SAPbobsCOM.BoObjectTypes.oOrders, "pedido");
                        }
                        else
                        {
                            Dialogs.Error("Erro interno. Erro ao selecionar os lotes do pedido.\nErro: " + ex.Message, true);
                        }
                    }
                    finally
                    {
                        oForm.Freeze(false);
                    }
                }
                else
                {
                    Dialogs.Error("Erro interno. Formulário não encontrado", true);
                }
            }
            else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && EventoDepois(pVal) && EventoEmAdicionandoNotaFiscalSaida(pVal) && pVal.ActionSuccess)
            {
                SAPbouiCOM.Form oForm = SBOApplication.Forms.Item(pVal.FormUID);
                if (oForm != null)
                {
                    oForm.Freeze(true);
                    try
                    {
                        if (oForm.DataSources.DBDataSources.Item("OINV").GetValue("DocStatus", 0) == "C")
                        {
                            Dialogs.Info("Validando cancelamento...");

                            SAPbobsCOM.Documents oNota = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                            if (oNota.Browser.GetByKeys(pVal.ObjectKey))
                            {
                                string nota_docentry = oNota.DocEntry.ToString();
                                string pedido_docentry = EncontraDocEntryPedidoBase(nota_docentry);
                                if (!String.IsNullOrEmpty(pedido_docentry))
                                {
                                    Dialogs.Info("Reabrindo o pedido de venda...");
                                    string status_cancelado = "RX";

                                    string update =
                                $@"DELETE FROM [@UPD_PCK_ETAPA] WHERE U_docentry = {pedido_docentry} AND U_status <> '{status_cancelado}' ;
							DELETE FROM [@UPD_PCK_OPERADOR] WHERE U_docentry = {pedido_docentry};
							DELETE FROM [@UPD_PCK_CONFPARCIAL] WHERE U_docentry = {pedido_docentry};
                            DELETE FROM [@UPD_PCK_VOLUMES] WHERE U_docentry = {pedido_docentry};
							DELETE FROM [@UPD_PCK_LOCAL] WHERE U_docentry = {pedido_docentry}; ";

                                    SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    try
                                    {
                                        // atualiza o campo de usuário, ele pode não existir.
                                        rs.DoQuery(update);

                                        update =
                                            "UPDATE ORDR SET U_UPD_PCK_STATUS = 'AS' WHERE DocEntry = " + pedido_docentry;

                                        rs.DoQuery(update);
                                    }
                                    catch (Exception) { }
                                }
                                else
                                {
                                    Dialogs.Error($"Erro interno. Pedido de Venda base não encontrado.", true);
                                }
                            }
                            else
                            {
                                Dialogs.Error($"Erro interno. Nota não encontrada.", true);
                                return;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Dialogs.Error("Erro interno. Erro ao tentar reabrir o pedido de venda relacionado a nota.\nErro: " + e.Message, true);
                    }
                    finally
                    {
                        oForm.Freeze(false);
                    }
                }
                else
                {
                    Dialogs.Error("Erro interno. Formulário não encontrado", true);
                }
            }
        }

        private void ItemEvent(string itemUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD && EventoAntes(pVal) && pVal.FormTypeEx == "139")
            {
                SAPbouiCOM.Form oForm = SBOApplication.Forms.GetFormByTypeAndCount(pVal.FormType, pVal.FormTypeCount);

                string refitemid = "2003";
                string cmbPrioridadeUID = "cmbPriori";

                SAPbouiCOM.Item refItem = oForm.Items.Item(refitemid);

                //***************************
                // Adding a Static Text item
                //***************************
                string nome_label = "Prioridade";
                SAPbouiCOM.Item oLabel = oForm.Items.Add("StaticTxt1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
                oLabel.FromPane = 0;
                oLabel.ToPane = 0;
                oLabel.Left = refItem.Left;
                oLabel.Width = 100;
                oLabel.Top = refItem.Top - 30;
                oLabel.Height = 14;
                //oLabel.RightJustified = true;

                oLabel.LinkTo = cmbPrioridadeUID;
                ((SAPbouiCOM.StaticText)(oLabel.Specific)).Caption = nome_label;

                SAPbouiCOM.Item itemPrioridade = oForm.Items.Add(cmbPrioridadeUID, SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);

                itemPrioridade.FromPane = 0;
                itemPrioridade.ToPane = 0;
                itemPrioridade.Top = oLabel.Top + 15;
                itemPrioridade.Left = oLabel.Left;
                itemPrioridade.Width = refItem.Width;

                SAPbouiCOM.ComboBox cmbPrioridade = itemPrioridade.Specific;

                itemPrioridade.DisplayDesc = true;
                itemPrioridade.Enabled = true;
                itemPrioridade.AffectsFormMode = true;

                cmbPrioridade.DataBind.SetBound(true, "ORDR", "U_UPD_PRIORIDADE");
                cmbPrioridade.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription;
            }
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD && EventoAntes(pVal) && pVal.FormTypeEx == "42")
            {
                SAPbouiCOM.Form oForm = SBOApplication.Forms.GetFormByTypeAndCount(pVal.FormType, pVal.FormTypeCount);

                string refitemid = "16";
                string btnSelAuthID = "btnSelAuth";

                SAPbouiCOM.Item refItem = oForm.Items.Item(refitemid);

                SAPbouiCOM.Item btnSelAuth = oForm.Items.Add(btnSelAuthID, SAPbouiCOM.BoFormItemTypes.it_BUTTON);

                btnSelAuth.FromPane = 0;
                btnSelAuth.ToPane = 0;
                btnSelAuth.Top = refItem.Top;
                btnSelAuth.Left = refItem.Left - 170;
                btnSelAuth.Width = refItem.Width + 60;

                SAPbouiCOM.Button btnSelecaoAutomatica = btnSelAuth.Specific;

                btnSelecaoAutomatica.Caption = "PICKING Seleção Automática";
            }
            else if (EventoAntes(pVal) && pVal.FormTypeEx == "42" && pVal.ItemUID == "btnSelAuth")
            {
                BubbleEvent = true;
                SAPbouiCOM.Form oForm = SBOApplication.Forms.GetFormByTypeAndCount(pVal.FormType, pVal.FormTypeCount);

                try
                {
                    SAPbouiCOM.Matrix mtxItens = oForm.Items.Item("3").Specific;
                    SAPbouiCOM.Matrix mtxLotesDisponiveis = oForm.Items.Item("4").Specific;
                    SAPbouiCOM.Matrix mtxLotesSelecionados = oForm.Items.Item("5").Specific;
                    SAPbouiCOM.Item btnDeselecionar = oForm.Items.Item("47");

                    for (int i = 1; i <= mtxItens.VisualRowCount; i++)
                    {
                        // selecionando a linha
                        mtxItens.Columns.Item("55").Cells.Item(i).Click();

                        for (int j = 1; j <= mtxLotesSelecionados.VisualRowCount; j++)
                        {
                            mtxLotesSelecionados.Columns.Item("1").Cells.Item(j).Click();
                            btnDeselecionar.Click();
                        }

                        // fazendo a ordenação da data
                        mtxLotesDisponiveis.Columns.Item("15").TitleObject.Click(SAPbouiCOM.BoCellClickType.ct_Double);
                        oForm.Items.Item("16").Click();
                    }
                }
                catch (Exception e)
                {
                    Dialogs.Error("Erro ao selecionar os lotes automaticamente.\nErro: " + e.Message, true);
                }
            }
        }


        #endregion


        #region :: Dispose

        public void Dispose()
        {
            SBOApplication = null;
            oCompany = null;
        }

        #endregion


        #region :: Utils

        public void SelecionarLotes(SAPbouiCOM.BusinessObjectInfo pVal, SAPbobsCOM.BoObjectTypes objType, string nome_objeto)
        {
            Dialogs.Info("Validando dados para seleção de lotes...");

            SAPbobsCOM.Documents oDoc = oCompany.GetBusinessObject(objType);
            if (oDoc.Browser.GetByKeys(pVal.ObjectKey))
            {
                try
                {
                    oCompany.StartTransaction();

                    string docentry = oDoc.DocEntry.ToString();

                    Dialogs.Info("Selecionando os lotes... Aguarde...");

                    int dias_venc = RetornaQtdDiasVencimento();
                    int total_itens = oDoc.Lines.Count;

                    if (total_itens == 0)
                    {
                        Dialogs.Error("Erro interno. Nenhum item foi encontrado para este " + nome_objeto + ".", true);
                    }

                    for (int i = 0; i < total_itens; i++)
                    {
                        Dialogs.Info("Selecionando os lotes... " + (i + 1) + " de " + total_itens + " itens.");

                        oDoc.Lines.SetCurrentLine(i);
                        string itemcode = oDoc.Lines.ItemCode;
                        string itemname = oDoc.Lines.ItemDescription;
                        string whscode = oDoc.Lines.WarehouseCode;
                        double qtd_pedido = oDoc.Lines.InventoryQuantity;

                        SAPbobsCOM.Items oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                        oItem.GetByKey(oDoc.Lines.ItemCode);

                        bool controlado_por_lote = oItem.ManageBatchNumbers == SAPbobsCOM.BoYesNoEnum.tYES;
                        bool controlado_por_num_serie = oItem.ManageSerialNumbers == SAPbobsCOM.BoYesNoEnum.tYES;

                        if (controlado_por_lote)
                        {

                            string sql =
                                $@"SELECT 
                                    tb1.DistNumber
                                    , tb2.Quantity - (CASE WHEN tb2.CommitQty IS NULL THEN 0 ELSE tb2.CommitQty END) as DISPONIVEL
                                FROM OBTN tb1
                                INNER JOIN OBTQ tb2 ON(tb1.ItemCode = tb2.ItemCode AND tb1.SysNumber = tb2.SysNumber)
                                WHERE tb2.WhsCode = '{whscode}' AND tb2.ItemCode = '{itemcode}'
                                    AND(tb2.Quantity - (CASE WHEN tb2.CommitQty IS NULL THEN 0 ELSE tb2.CommitQty END)) > 0.00
                                    AND (
                                            tb1.EXPDATE >= GETDATE()	+ ({dias_venc} - 1) --O LOTE NÃO PODE ESTAR VENCIDO
		                                    OR 
                                            tb1.EXPDATE IS NULL
                                        )
                                ORDER BY tb1.ExpDate, tb1.SysNumber ASC ";

                            SAPbobsCOM.Recordset rsLote = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            rsLote.DoQuery(sql);

                            if (rsLote.RecordCount == 0)
                            {
                                Dialogs.Error("Erro! Nenhum lote encontrado para o item '" + itemcode + " - " + itemname + "'.", true);
                            }
                            else
                            {
                                // percorrendo todos os lotes do item
                                while (!rsLote.EoF)
                                {
                                    double qtd_alocada = qtd_pedido;

                                    string num_lote = rsLote.Fields.Item("DistNumber").Value;
                                    double disponivel_lote = rsLote.Fields.Item("DISPONIVEL").Value;

                                    if (qtd_pedido > disponivel_lote)
                                    {
                                        qtd_alocada = disponivel_lote;
                                    }

                                    qtd_pedido = qtd_pedido - qtd_alocada;

                                    oDoc.Lines.BatchNumbers.Quantity = qtd_alocada;
                                    oDoc.Lines.BatchNumbers.BatchNumber = num_lote;
                                    oDoc.Lines.BatchNumbers.Add();

                                    if (qtd_pedido == 0)
                                    {
                                        break;
                                    }

                                    rsLote.MoveNext();
                                }
                            }
                        }
                        else if (controlado_por_num_serie)
                        {
                            string sql =
                                $@"SELECT tb1.DistNumber, tb1.MnfSerial, tb1.SysNumber
                                            FROM OSRN tb1 
                                            INNER JOIN OSRQ tb2 ON tb1.ItemCode = tb2.ItemCode AND tb1.SysNumber = tb2.SysNumber
                                            WHERE tb2.WhsCode = '{whscode}' AND tb2.ItemCode = '{itemcode}'
                                                AND (tb2.Quantity - (CASE WHEN tb2.CommitQty IS NULL THEN 0 ELSE tb2.CommitQty END)) > 0.00  ORDER BY tb1.InDate ASC";

                            SAPbobsCOM.Recordset rsNumSerie = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            rsNumSerie.DoQuery(sql);

                            if (rsNumSerie.RecordCount == 0)
                            {
                                Dialogs.Error("Erro! Nenhum número de série encontrado para o item '" + itemcode + " - " + itemname + "'.", true);
                            }
                            else
                            {
                                while (!rsNumSerie.EoF && qtd_pedido > 0)
                                {
                                    oDoc.Lines.SerialNumbers.Quantity = 1;
                                    oDoc.Lines.SerialNumbers.SystemSerialNumber = rsNumSerie.Fields.Item("SysNumber").Value;
                                    oDoc.Lines.SerialNumbers.Add();

                                    qtd_pedido = qtd_pedido - 1;

                                    rsNumSerie.MoveNext();
                                }
                            }
                        }
                        else
                        {
                            Dialogs.Error("Erro de Configuração! O Item '" + itemcode + " - " + itemname + "' não está configurado para ser administrado por número de série ou por lote.", true);
                        }

                        SBOApplication.RemoveWindowsMessage(SAPbouiCOM.BoWindowsMessageType.bo_WM_TIMER, true);
                    }

                    if (oDoc.Update() == 0)
                    {
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    }
                    else
                    {
                        Dialogs.Error("Erro ao selecionar os lotes.\nErro: " + oCompany.GetLastErrorDescription(), true);
                    }
                }
                catch (Exception e)
                {
                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    throw e;
                }
                finally
                {
                    try
                    {
                        // atualiza garantindo o campo de usuário
                        var rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        rs.DoQuery($"UPDATE ORDR SET U_UPD_PCK_STATUS = 'AS' WHERE DocEntry = {oDoc.DocEntry}");
                    }
                    catch (Exception e) { }
                }
            }
            else
            {
                Dialogs.Error($"Erro interno. Número do " + nome_objeto + " não encontrado.", true);
            }
        }

        public bool EventoAntes(SAPbouiCOM.BusinessObjectInfo pVal)
        {
            return pVal.BeforeAction;
        }

        public bool EventoDepois(SAPbouiCOM.BusinessObjectInfo pVal)
        {
            return !pVal.BeforeAction;
        }

        public bool EventoAntes(SAPbouiCOM.MenuEvent pVal)
        {
            return pVal.BeforeAction;
        }

        public bool EventoDepois(SAPbouiCOM.MenuEvent pVal)
        {
            return !pVal.BeforeAction;
        }

        public bool EventoAntes(SAPbouiCOM.ItemEvent pVal)
        {
            return pVal.BeforeAction;
        }

        public bool EventoDepois(SAPbouiCOM.ItemEvent pVal)
        {
            return !pVal.BeforeAction;
        }

        private bool EventoEmAdicionandoPedidoVenda(SAPbouiCOM.BusinessObjectInfo pVal)
        {
            return pVal.Type == ((int)SAPbobsCOM.BoObjectTypes.oOrders).ToString();
        }

        private bool EventoEmAdicionandoNotaFiscalSaida(SAPbouiCOM.BusinessObjectInfo pVal)
        {
            return pVal.Type == ((int)SAPbobsCOM.BoObjectTypes.oInvoices).ToString();
        }

        private bool EventoEmAdicionandoEntrega(SAPbouiCOM.BusinessObjectInfo pVal)
        {
            return pVal.Type == ((int)SAPbobsCOM.BoObjectTypes.oDeliveryNotes).ToString();
        }

        private string EncontraDocEntryPedidoBase(string nota_docentry)
        {
            string sql =
                $@"SELECT 
	                    TOP(1) tb3.BaseRef 
                    FROM INV1 tb1
                    INNER JOIN OINV tb2 ON ( tb2.DocNum = tb1.BaseRef )
                    INNER JOIN INV1 tb3 ON ( tb3.DocEntry = tb2.DocEntry )
                    WHERE tb1.DocEntry =  " + nota_docentry;
            SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rs.DoQuery(sql);
            return rs.Fields.Item("BaseRef").Value;
        }

        public bool JaGerouPicking(string docentry)
        {
            string sql =
                $@"SELECT 
                    COUNT(*) as count
                FROM PKL1 tb0
                INNER JOIN OPKL tb1 ON(tb1.AbsEntry = tb0.AbsEntry)
                WHERE OrderEntry = {docentry} AND ( tb0.PickStatus = 'R' OR tb0.PickStatus = 'Y' ) ";

            SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rs.DoQuery(sql);
            return ((int)rs.Fields.Item("count").Value > 0);
        }

        public int RetornaQtdDiasVencimento()
        {
            int res = 0;

            SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rs.DoQuery("SELECT U_US_QTDDIAS 'Qdias' FROM[@U_CONF] WHERE CODE = 1 ");
            res = (int)rs.Fields.Item("Qdias").Value;

            return res;
        }

        public bool UsuarioLogadoEVendedor()
        {
            SAPbobsCOM.Recordset rs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rs.DoQuery(
                $@"SELECT 
                        tb0.empID
                    FROM OHEM tb0
                    INNER JOIN HEM6 tb1 ON (tb1.empID = tb0.empID)
                    INNER JOIN OUSR tb2 ON (tb2.USERID = tb0.USERID)
                    WHERE ROLEID = -3 AND tb2.USER_CODE = '{oCompany.UserName}'"
            );

            return rs.RecordCount > 0;
        }

        private void SalvarTXT(string data)
        {
            string path = Path.GetPathRoot(Environment.SystemDirectory) + "/tmp/";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string file_name = oCompany.UserName + "_addon_selecao_lotes_log.txt";
            string fullpath = path + file_name;

            try
            {
                using (StreamWriter sw = new StreamWriter(fullpath, true))
                {
                    sw.WriteLine(data);
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Dialogs.Error("Erro interno. Erro ao salvar arquivo de log de seleção de lote.\nErro: " + e.Message, true);
            }
        }

        #endregion
    }
}
