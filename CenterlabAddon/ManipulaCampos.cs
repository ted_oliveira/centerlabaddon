﻿using Fast.SAP.DataBase;
using Fast.SAP.DataBase.Enums;
using SAPbouiCOM;
using System;

namespace CenterlabAddon
{
    public class ManipulaCampos
    {

        #region "Declarações"
        private SAPbouiCOM.Application sboApplication;
        private SAPbobsCOM.Company oCompany;

        private SAPbobsCOM.Recordset sboRecordSet;
        private string strQuery = null;
        private Boolean InsertTabelaCalcDiaria = false;

        private string MsgErroBD;  // Mensagem de Erro do Banco de Dados
        private int CodErroBD;     // Código de Erro do Banco de Dados
        private SAPbobsCOM.Company company;
        private FastDataBaseManager fastDataBaseManager;
        #endregion

        public ManipulaCampos(Application sboApplication, SAPbobsCOM.Company company)
        {
            this.sboApplication = sboApplication;
            this.company = company;
            fastDataBaseManager = new FastDataBaseManager(company, sboApplication);
        }


        /// <summary>
        /// Cria ou modifica a base de dados para aplicar funcionalidades do add-on
        /// </summary>
        public void CriarCamposAutomaticamente()
        {
            string tableName;
            string description;
            TIPOTABELAEnum tipoTabela;

            tableName = "ORDR";
            description = string.Empty;
            tipoTabela = TIPOTABELAEnum.tSBO;

            tableName = "OITW";

            fastDataBaseManager.CriarCampo(tableName, "ZTH_Loc_Fisico", "Local Fisico", TIPOCAMPOEnum.ALFANUM, 20, tipoTabela);

            tableName = "U_CONF";
            description = string.Empty;
            tipoTabela = TIPOTABELAEnum.tUSER;

            bool insertSqlFields = !fastDataBaseManager.ExisteTabela(tableName);

            if (insertSqlFields)
            {
                fastDataBaseManager.CriarTabela(tableName, description, TIPOOBJETOEnum.tNOOBJECT);
            }

            fastDataBaseManager.CriarCampo(tableName, "US_QTDDIAS", "Dias vencimento", TIPOCAMPOEnum.NUMERO, 10, tipoTabela);

            if (insertSqlFields)
            {
                Fast.SAP.DataBase.Utils.FastDataBaseUtils.ExecuteQuery(company, "INSERT INTO [@U_CONF] (CODE, NAME, U_US_QTDDIAS) VALUES (1, 'MAIN', 0)");
            }
        }
    }
}
